package infrastructure

import (
	"context"
	"diploma/internal/controllers"
	"diploma/internal/environment"
	"diploma/internal/interfaces"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"net/http"
	"time"
)

type kernel struct {
	logger     *zap.SugaredLogger
	httpClient *http.Client
	DbHandler  interfaces.IDBHandler
}

type IInjector interface {
	InjectAuthController() *controllers.AuthController
	InjectGroupController() *controllers.GroupController
	InjectTaskController() *controllers.TaskController
}

func (k *kernel) InjectAuthController() *controllers.AuthController {
	return &controllers.AuthController{
		Log:            k.logger,
		AuthService:    k.InjectAuthService(),
		GroupService:   k.InjectGroupService(),
		SubjectService: k.InjectSubjectService(),
	}
}

func (k *kernel) InjectGroupController() *controllers.GroupController {
	return &controllers.GroupController{
		Log:          k.logger,
		GroupService: k.InjectGroupService(),
	}
}

func (k *kernel) InjectTaskController() *controllers.TaskController {
	return &controllers.TaskController{
		Log:           k.logger,
		TaskService:   k.InjectTaskService(),
		AnswerService: k.InjectAnswerService(),
	}
}

func Inject(env environment.Environment, ctx context.Context, logger *zap.SugaredLogger) (k *kernel, err error) {
	k = &kernel{}
	k.logger = logger
	k.httpClient = &http.Client{
		Timeout: time.Second * 30,
	}
	postgresClient, err := initPostgresClient(ctx, env.Postgres)
	if err != nil {
		return nil, errors.Wrap(err, "Inject")
	}
	k.DbHandler = postgresClient

	migrationResult, err := postgresClient.ApplyMigration(env.FormConnStringPg(), "/migrations")
	if err != nil {
		return nil, errors.Wrap(err, "Inject")
	}
	logger.Infof("Migration applied. Result: %s", migrationResult)
	return k, nil
}
