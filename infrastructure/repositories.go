package infrastructure

import (
	"diploma/internal/interfaces"
	"diploma/internal/repositories"
	"os"
)

func (k *kernel) InjectTaskRepository() interfaces.ITaskRepository {
	return repositories.TaskRepository{
		FilePath: "http://" + os.Getenv("APP_ADDR") + ":" + os.Getenv("APP_PORT") + "/public/tasks/",
	}
}

func (k *kernel) InjectAnswerRepository() interfaces.IAnswerRepository {
	return repositories.AnswerRepository{
		FilePath: "http://" + os.Getenv("APP_ADDR") + ":" + os.Getenv("APP_PORT") + "/public/answers/",
	}
}
