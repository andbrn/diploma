package infrastructure

import (
	"diploma/internal/repositories"
	"diploma/internal/services"
)

func (k *kernel) InjectAuthService() *services.AuthService {
	return &services.AuthService{
		IDBHandler:     k.DbHandler,
		AuthRepository: &repositories.AuthRepository{},
	}
}

func (k *kernel) InjectGroupService() *services.GroupService {
	return &services.GroupService{
		IDBHandler:      k.DbHandler,
		GroupRepository: &repositories.GroupRepository{},
	}
}

func (k *kernel) InjectTaskService() *services.TaskService {
	return &services.TaskService{
		IDBHandler:       k.DbHandler,
		TaskRepository:   k.InjectTaskRepository(),
		UserRepository:   &repositories.UserRepository{},
		GroupRepository:  &repositories.GroupRepository{},
		AnswerRepository: k.InjectAnswerRepository(),
	}
}

func (k *kernel) InjectSubjectService() *services.SubjectService {
	return &services.SubjectService{
		IDBHandler:        k.DbHandler,
		SubjectRepository: &repositories.SubjectRepository{},
	}
}

func (k *kernel) InjectAnswerService() *services.AnswerService {
	return &services.AnswerService{
		IDBHandler:        k.DbHandler,
		AnswerRepository:  k.InjectAnswerRepository(),
		TaskRepository:    k.InjectTaskRepository(),
		UserRepository:    &repositories.UserRepository{},
		SubjectRepository: &repositories.SubjectRepository{},
	}
}
