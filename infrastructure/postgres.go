package infrastructure

import (
	"context"
	"diploma/internal/environment"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"log"
	"time"
)

type PostgresDatabaseClient struct {
	pool *pgxpool.Pool
}

func initPostgresClient(ctx context.Context, env environment.Postgres) (*PostgresDatabaseClient, error) {
	connString := env.FormConnStringPg()
	log.Printf("init postgres connString: %s", connString)

	cfg, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return &PostgresDatabaseClient{}, errors.Wrap(err, "initPostgresClient")
	}

	pool, err := pgxpool.ConnectConfig(ctx, cfg)
	if err != nil {
		return &PostgresDatabaseClient{}, errors.Wrap(err, "initPostgresClient")
	}

	return &PostgresDatabaseClient{pool: pool}, nil
}

func (p *PostgresDatabaseClient) ApplyMigration(databaseURL, migrationPath string) (result string, err error) {
	m, err := migrate.New(
		fmt.Sprintf("file://%s", migrationPath),
		databaseURL)
	if err != nil {
		return result, errors.Wrap(err, "applyMigrations")
	}
	defer func() {
		if err1, err2 := m.Close(); err1 != nil || err2 != nil {
			log.Println("errors closing migrate", err1, err2)
		}
	}()
	defer func() {
		version, dirty, err := m.Version()
		if err != nil {
			log.Println("error getting migrate version", err)
			return
		}
		result = fmt.Sprintf("Version: %d Dirty: %t", version, dirty)
	}()

	if err = m.Up(); err != nil {
		if err == migrate.ErrNoChange {
			return result, nil
		}
		return result, errors.Wrap(err, "applyMigrations")
	}

	return
}

func (p *PostgresDatabaseClient) AcquireConn(ctx context.Context) (pool *pgxpool.Conn, err error) {
	if pool, err = p.pool.Acquire(ctx); err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	return
}

func (p *PostgresDatabaseClient) StartTransaction(ctx context.Context) (tx pgx.Tx, err error) {
	tx, err = p.pool.Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "StartTransaction")
	}
	return
}

func (p *PostgresDatabaseClient) FinishTransaction(ctx context.Context, tx pgx.Tx, err error) error {
	if err != nil {
		if rollbackErr := tx.Rollback(ctx); rollbackErr != nil {
			return errors.Wrap(err, rollbackErr.Error())
		}

		return err
	}

	if commitErr := tx.Commit(ctx); commitErr != nil {
		return commitErr
	}

	return nil
}

func (p *PostgresDatabaseClient) Close() {
	defer func(t time.Time) { log.Printf("pool Close took: %f", time.Now().Sub(t).Seconds()) }(time.Now())
	p.pool.Close()
}
