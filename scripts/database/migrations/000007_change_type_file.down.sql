ALTER TABLE files_tasks DROP COLUMN file_link;
ALTER TABLE files_tasks DROP COLUMN file_name;
ALTER TABLE files_answers DROP COLUMN file_link;
ALTER TABLE files_answers DROP COLUMN file_name;

ALTER TABLE files_tasks ADD COLUMN file_link INT4 NOT NULL;
ALTER TABLE files_tasks ADD COLUMN file_name INT4 NOT NULL;
ALTER TABLE files_answers ADD COLUMN file_link INT4 NOT NULL;
ALTER TABLE files_answers ADD COLUMN file_name INT4 NOT NULL;
