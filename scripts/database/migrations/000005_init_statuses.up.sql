ALTER TABLE statuses DROP COLUMN previous_status_id;
ALTER TABLE statuses DROP COLUMN next_status_id;

INSERT INTO statuses(id, status_en, status_ru)
VALUES (1, 'to_do', 'Очистить статус');

INSERT INTO statuses(id, status_en, status_ru)
VALUES (2, 'in_progress', 'Начать делать');

INSERT INTO statuses(id, status_en, status_ru)
VALUES (3, 'in_review', 'На проверку');

INSERT INTO statuses(id, status_en, status_ru)
VALUES (4, 'marked', 'Оценить');
