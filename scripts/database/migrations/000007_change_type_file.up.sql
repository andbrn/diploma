ALTER TABLE files_tasks DROP COLUMN file_link;
ALTER TABLE files_tasks DROP COLUMN file_name;
ALTER TABLE files_answers DROP COLUMN file_link;
ALTER TABLE files_answers DROP COLUMN file_name;

ALTER TABLE files_tasks ADD COLUMN file_link TEXT NOT NULL;
ALTER TABLE files_tasks ADD COLUMN file_name TEXT NOT NULL;
ALTER TABLE files_answers ADD COLUMN file_link TEXT NOT NULL;
ALTER TABLE files_answers ADD COLUMN file_name TEXT NOT NULL;
