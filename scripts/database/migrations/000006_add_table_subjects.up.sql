CREATE TABLE IF NOT EXISTS subjects (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS teachers_subjects(
    subject_id INT4,
    teacher_id INT4
);

ALTER TABLE tasks ADD COLUMN subject_id INT4 NOT NULL DEFAULT 1;
ALTER TABLE tasks DROP COLUMN IF EXISTS subject;
