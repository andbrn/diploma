CREATE TABLE IF NOT EXISTS study_groups
(
    id SERIAL PRIMARY KEY,
    group_name TEXT NOT NULL,
    faculty TEXT NOT NULL,
    direction TEXT NOT NULL,
    main BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE IF NOT EXISTS student_subgroup
(
    student_id INT4 NOT NULL,
    subgroup_id INT4 NOT NULL
);

CREATE TABLE IF NOT EXISTS tasks
(
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    subtitle TEXT NULL,
    start_date TIMESTAMPTZ NOT NULL,
    end_date TIMESTAMPTZ NOT NULL,
    description TEXT NULL
);

CREATE TABLE IF NOT EXISTS statuses
(
    id SERIAL PRIMARY KEY,
    previous_status_id INT4 NULL,
    next_status_id INT4 NULL,
    status_ru TEXT NULL,
    status_en TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS answers
(
    id SERIAL PRIMARY KEY,
    task_id INT4 NOT NULL,
    student_id INT4 NOT NULL,
    status_id INT4 NOT NULL,
    comment TEXT NULL,
    mark NUMERIC(3, 1) NULL
);

CREATE TABLE IF NOT EXISTS files_answers
(
    answer_id INT4 NOT NULL,
    file_link INT4 NOT NULL,
    file_name INT4 NOT NULL
);


CREATE TABLE IF NOT EXISTS files_tasks
(
    task_id INT4 NOT NULL,
    file_link INT4 NOT NULL,
    file_name INT4 NOT NULL
);

CREATE TABLE IF NOT EXISTS users
(
    id SERIAL PRIMARY KEY,
    is_student BOOL NOT NULL DEFAULT true,
    first_name TEXT NOT NULL,
    middle_name TEXT NULL,
    last_name TEXT NOT NULL,
    position_name TEXT NULL,  -- for teachers
    image_link TEXT NULL,
    email TEXT NOT NULL,
    phone TEXT NULL,
    password TEXT NOT NULL,
    group_id INT4 NULL -- for students
);

CREATE TABLE IF NOT EXISTS tokens
(
    user_id INT4 NOT NULL,
    token UUID NOT NULL DEFAULT gen_random_uuid(),
    created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);
