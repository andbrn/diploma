DELETE FROM statuses;

ALTER TABLE statuses ADD COLUMN previous_status_id INT4 NULL;
ALTER TABLE statuses ADD COLUMN next_status_id INT4 NULL;
