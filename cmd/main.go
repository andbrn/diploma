package main

import (
	"context"
	"diploma/infrastructure"
	"diploma/internal/controllers"
	"diploma/internal/environment"
	"fmt"
	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var (
	Log             *zap.SugaredLogger
	version, date   string
	env             environment.Environment
	authController  *controllers.AuthController
	groupController *controllers.GroupController
	taskController  *controllers.TaskController
	ctx             = context.Background()
)

func init() {
	fmt.Printf("Building date: %s\n", date)

	logger, err := zap.Config{
		Level:            zap.NewAtomicLevelAt(zap.DebugLevel),
		Development:      true,
		Encoding:         "console",
		EncoderConfig:    zap.NewDevelopmentEncoderConfig(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}.Build(zap.Fields(zapcore.Field{
		Type:   zapcore.StringType,
		Key:    "BuildInfo",
		String: fmt.Sprintf("%s, %s", version, date),
	}))
	if err != nil {
		fmt.Printf("error loading logger: %s", err)
		os.Exit(1)
	}
	Log = logger.Sugar()
	env = environment.NewEnvironment()
	Log.Infof("env loaded: %+v", env)
}

func main() {
	router := echo.New()

	router.Use(middleware.Logger())
	router.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:8080", "http://194.67.112.23:8080", "http://194.67.112.23"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	router.Validator = &CustomValidator{*validator.New()}

	kernel, err := infrastructure.Inject(env, ctx, Log)
	if err != nil {
		Log.Fatal(err)
	}

	registerRoutes(router, kernel)
	Log.Fatal(router.Start(":1001"))
}

func registerRoutes(router *echo.Echo, injector infrastructure.IInjector) {
	authController = injector.InjectAuthController()
	groupController = injector.InjectGroupController()
	taskController = injector.InjectTaskController()
	api := router.Group("/api/v1", authMiddleware)
	{
		auth := api.Group("/auth")
		{
			auth.POST("/token", authController.ProcessAuth)
		}
		user := api.Group("/user")
		{
			user.GET("/info", authController.TakeInfo)
		}
		group := api.Group("/group")
		{
			group.GET("/list", groupController.GroupList)
		}
		task := api.Group("/task")
		{
			task.POST("/new", taskController.NewTask)
			task.POST("/file/:taskId", taskController.UploadFiles)
			task.GET("/journal", taskController.Journal)
			task.GET("/diary", taskController.Diary)
			task.POST("/status", taskController.ChangeStatus)
			task.POST("/mark", taskController.SetMark)
			task.GET("/review", taskController.Review)
		}
		api.GET("/board", taskController.TasksToStudent)
	}
	router.Static("/public", "/public")
}

type CustomValidator struct {
	Validator validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.Validator.Struct(i)
}

func authMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if ctx.Path() == "/api/v1/auth/token" {
			return next(ctx)
		}
		header := strings.Split(ctx.Request().Header.Get("Authorization"), " ")
		if len(header) < 2 {
			Log.Error("empty header ", ctx.Request().RequestURI)
			return ctx.JSON(http.StatusUnauthorized, echo.Map{
				"error": "Authorization Header is empty",
			})
		}
		userId, err := strconv.Atoi(header[0])
		if err != nil {
			Log.Error("userId is not a number ", ctx.Request().RequestURI)
			return ctx.JSON(http.StatusUnauthorized, echo.Map{
				"error": "userId is not a number",
			})
		}
		sc, isStudent, err := authController.AuthService.CheckAuth(int32(userId), header[1])
		if err != nil {
			Log.Error(err.Error(), ctx.Request().RequestURI)
			return ctx.JSON(sc, echo.Map{
				"error": err.Error(),
			})
		}
		if sc != http.StatusOK {
			Log.Error(fmt.Sprintf("non 200 (%d) sc", sc), ctx.Request().RequestURI)
			return ctx.JSON(sc, echo.Map{
				"error": fmt.Sprintf("non 200 (%d) sc", sc),
			})
		}
		ctx.Set("isStudent", isStudent)
		return next(ctx)
	}
}
