package environment

import (
	"fmt"
	"os"
	"runtime"
)

type Environment struct {
	Postgres
}

type Postgres struct {
	Username, Password, Host, Port, Database string
}

func (e *Postgres) fillFromEnv() {
	e.Username = os.Getenv("POSTGRES_USER")
	e.Password = os.Getenv("POSTGRES_PASSWORD")
	e.Host = os.Getenv("POSTGRES_HOST")
	e.Port = os.Getenv("POSTGRES_PORT")
	e.Database = os.Getenv("POSTGRES_DB")
}

func (e Postgres) FormConnStringPg() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", e.Username, e.Password, e.Host, e.Port, e.Database)
}

func (e Postgres) FormConnStringPgWithPool() string {
	poolSize := runtime.NumCPU() * 4
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable&pool_max_conns=%d", e.Username, e.Password, e.Host, e.Port, e.Database, poolSize)
}

func NewEnvironment() (e Environment) {
	e.Postgres.fillFromEnv()
	return
}
