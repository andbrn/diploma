package repositories

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"fmt"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
)

type AnswerRepository struct {
	FilePath string
}

func (r AnswerRepository) AddAnswers(ctx context.Context, querier interfaces.IQuerier, studentIds []pgtype.Int4,
	taskId pgtype.Int4) (err error) {
	const query = `
		INSERT INTO answers(task_id, student_id, status_id)
		VALUES ($1, $2, 1)
	`
	b := &pgx.Batch{}
	for _, studentId := range studentIds {
		b.Queue(query, taskId, studentId)
	}
	bResults := querier.SendBatch(ctx, b)
	for i := 0; i < b.Len(); i++ {
		if _, err = bResults.Exec(); err != nil {
			return errors.Wrap(err, "Exec")
		}
	}
	defer func(bResults pgx.BatchResults) {
		errB := bResults.Close()
		if errB != nil {
			err = errors.Wrap(err, errB.Error())
		}
	}(bResults)
	return
}

func (r AnswerRepository) SelectAnswersByGroupAndSubject(ctx context.Context, querier interfaces.IQuerier,
	teacherId, groupId, subjectId pgtype.Int4) (answers []dao.CellAnswer, err error) {
	const query = `
		SELECT a.comment, a.mark, a.id, s.status_en, t.start_date, t.end_date,
        t.title, t.subtitle, t.description, a.student_id, a.status_id, t.id
		FROM answers a JOIN tasks t on a.task_id = t.id
                       JOIN statuses s on a.status_id = s.id
        WHERE t.group_id = $1 AND t.subject_id=$2 AND t.teacher_id=$3
		ORDER BY t.start_date;
	`
	rows, err := querier.Query(ctx, query, groupId, subjectId, teacherId)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	answers = make([]dao.CellAnswer, 0, 10)
	for rows.Next() {
		var answer dao.CellAnswer
		var mark pgtype.Numeric
		if err = rows.Scan(&answer.Comment, &mark, &answer.Id, &answer.Status, &answer.StartDate,
			&answer.EndDate, &answer.Title, &answer.SubTittle, &answer.Description, &answer.PersonId,
			&answer.StatusId, &answer.TaskId); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		var text float32
		if mark.Status != pgtype.Null {
			if err = mark.AssignTo(&text); err != nil {
				return nil, errors.Wrap(err, "AssignTo")
			}
			if err = answer.Text.Set(fmt.Sprint(text)); err != nil {
				return nil, errors.Wrap(err, "Set")
			}
		} else if err = answer.Text.Set(answer.Status.String); err != nil {
			return nil, errors.Wrap(err, "Set")
		}
		answers = append(answers, answer)
	}
	return
}

func (r AnswerRepository) ChangeStatus(ctx context.Context, querier interfaces.IQuerier, status, comment pgtype.Text,
	answerId pgtype.Int4) (err error) {
	const query = `
		UPDATE answers
		SET status_id = (
			SELECT id
			FROM statuses
			WHERE status_en = $1
			),
			comment=$2
		WHERE id = $3;
	`
	if _, err = querier.Exec(ctx, query, status, comment, answerId); err != nil {
		return errors.Wrap(err, "Exec")
	}
	return
}

func (r AnswerRepository) SetMark(ctx context.Context, querier interfaces.IQuerier, mark pgtype.Numeric,
	comment pgtype.Text, answerId pgtype.Int4) (err error) {
	const query = `
		UPDATE answers
		SET status_id = 4,
			mark = $1,
			comment=$2
		WHERE id = $3;
	`
	if _, err = querier.Exec(ctx, query, mark, comment, answerId); err != nil {
		return errors.Wrap(err, "Exec")
	}
	return
}

func (r AnswerRepository) TakeFiles(ctx context.Context, querier interfaces.IQuerier,
	answerId pgtype.Int4) (files []pgtype.Text, err error) {
	const query = `
		SELECT file_link FROM files_answers
		WHERE answer_id = $1
	`
	var rows pgx.Rows
	if rows, err = querier.Query(ctx, query, answerId); err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	files = make([]pgtype.Text, 0, 3)
	for rows.Next() {
		var file pgtype.Text
		if err = rows.Scan(&file); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		files = append(files, file)
	}
	return
}

func (r AnswerRepository) InsertFileName(ctx context.Context, querier interfaces.IQuerier, answerId pgtype.Int4,
	filename pgtype.Text) (fileLink string, err error) {
	const query = `
		INSERT INTO files_answers (answer_id, file_link, file_name)
		VALUES ($1, $2, $3);
	`
	fileLink = r.FilePath + filename.String
	var fileLinkDb pgtype.Text
	if err = fileLinkDb.Set(fileLink); err != nil {
		return "", errors.Wrap(err, "Set")
	}
	if _, err = querier.Exec(ctx, query, answerId, fileLinkDb, filename); err != nil {
		return "", errors.Wrap(err, "Exec")
	}
	return
}

func (r AnswerRepository) TakeDiaryDates(ctx context.Context, querier interfaces.IQuerier,
	studentId pgtype.Int4) (dates []pgtype.Date, err error) {
	const query = `
		SELECT DISTINCT DATE(t.start_date) FROM answers a
			JOIN tasks t ON t.id = a.task_id
		WHERE a.student_id = $1
		ORDER BY DATE(t.start_date);
	`
	rows, err := querier.Query(ctx, query, studentId)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	dates = make([]pgtype.Date, 0, 20)
	for rows.Next() {
		var date pgtype.Date
		if err = rows.Scan(&date); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		dates = append(dates, date)
	}
	return
}

func (r AnswerRepository) SelectAnswersByStudent(ctx context.Context, querier interfaces.IQuerier,
	studentId pgtype.Int4) (answers []dao.CellAnswer, err error) {
	const query = `
		SELECT a.comment, a.mark, a.id, s.status_en, t.start_date, t.end_date,
        t.title, t.subtitle, t.description, t.teacher_id, a.status_id, t.id, t.subject_id
		FROM answers a JOIN tasks t on a.task_id = t.id
                       JOIN statuses s on a.status_id = s.id
        WHERE a.student_id = $1;
	`
	var rows pgx.Rows
	if rows, err = querier.Query(ctx, query, studentId); err != nil {
		return nil, errors.Wrap(err, "Exec")
	}
	answers = make([]dao.CellAnswer, 0, 20)
	for rows.Next() {
		var (
			answer dao.CellAnswer
			mark   pgtype.Numeric
		)
		if err = rows.Scan(&answer.Comment, &mark, &answer.Id, &answer.Status,
			&answer.StartDate, &answer.EndDate, &answer.Title, &answer.SubTittle,
			&answer.Description, &answer.PersonId, &answer.StatusId, &answer.TaskId, &answer.SubjectId); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}

		var text float32
		if mark.Status != pgtype.Null {
			if err = mark.AssignTo(&text); err != nil {
				return nil, errors.Wrap(err, "AssignTo")
			}
			if err = answer.Text.Set(fmt.Sprint(text)); err != nil {
				return nil, errors.Wrap(err, "Set")
			}
		} else if err = answer.Text.Set(answer.Status.String); err != nil {
			return nil, errors.Wrap(err, "Set")
		}
		answers = append(answers, answer)
	}
	return
}
