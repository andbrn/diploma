package repositories

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"net/http"
	"time"
)

type AuthRepository struct {
}

func (r AuthRepository) Auth(ctx context.Context, querier interfaces.IQuerier, email string,
	password string) (userId pgtype.Int4, err error) {
	const query = `
		SELECT id FROM users
		WHERE email = $1 AND password = $2;
	`
	if err = querier.QueryRow(ctx, query, email, password).Scan(&userId); err != nil {
		return pgtype.Int4{}, errors.Wrap(err, "TakeToken")
	}
	return
}

func (r AuthRepository) GenerateToken(ctx context.Context, querier interfaces.IQuerier,
	userId pgtype.Int4) (token pgtype.UUID, err error) {
	const query = `
		INSERT INTO tokens (user_id)
		VALUES ($1)
		RETURNING token;
	`
	if err = querier.QueryRow(ctx, query, userId).Scan(&token); err != nil {
		return pgtype.UUID{}, errors.Wrap(err, "TakeToken")
	}
	return
}

func (r AuthRepository) CheckAuth(ctx context.Context, querier interfaces.IQuerier, userId pgtype.Int4,
	token pgtype.UUID) (sc int, err error) {
	const query = `
		SELECT created_at FROM tokens
		WHERE user_id = $1 AND token = $2; 
	`
	var (
		createdAtDb pgtype.Timestamptz
		createdAt   time.Time
	)
	if err = querier.QueryRow(ctx, query, userId, token).Scan(&createdAtDb); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return http.StatusUnauthorized, nil
		}
		return http.StatusInternalServerError, errors.Wrap(err, "CheckAuth")
	}
	if err = createdAtDb.AssignTo(&createdAt); err != nil {
		return http.StatusInternalServerError, errors.Wrap(err, "CheckAuth")
	}
	if time.Since(createdAt) > time.Hour*24*60 {
		return http.StatusForbidden, nil
	}
	return http.StatusOK, nil
}

func (r AuthRepository) GetInfoByEmail(ctx context.Context, querier interfaces.IQuerier,
	email pgtype.Text) (user dao.User, err error) {
	const query = `
		SELECT id, is_student, first_name, middle_name, last_name, position_name, image_link, email, phone, group_id
		FROM users WHERE email = $1;
	`
	if err = querier.QueryRow(ctx, query, email).Scan(&user.Id, &user.IsStudent, &user.FirstName, &user.MiddleName,
		&user.LastName, &user.PositionName, &user.ImageLink, &user.Email, &user.Phone, &user.GroupId); err != nil {
		return user, errors.Wrap(err, "GetInfoByEmail")
	}
	return
}

func (r AuthRepository) GetInfoById(ctx context.Context, querier interfaces.IQuerier,
	id pgtype.Int4) (user dao.User, err error) {
	const query = `
		SELECT id, is_student, first_name, middle_name, last_name, position_name, image_link, email, phone, group_id
		FROM users WHERE id = $1;
	`
	if err = querier.QueryRow(ctx, query, id).Scan(&user.Id, &user.IsStudent, &user.FirstName, &user.MiddleName,
		&user.LastName, &user.PositionName, &user.ImageLink, &user.Email, &user.Phone, &user.GroupId); err != nil {
		return user, errors.Wrap(err, "GetInfoById")
	}
	return
}

func (r AuthRepository) IsStudent(ctx context.Context, querier interfaces.IQuerier,
	userId pgtype.Int4) (isStudent bool, err error) {
	const query = `
		SELECT is_student
		FROM users WHERE id = $1;
	`
	if err = querier.QueryRow(ctx, query, userId).Scan(&isStudent); err != nil {
		return false, errors.Wrap(err, "GetInfoById")
	}
	return
}
