package repositories

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type UserRepository struct {
}

func (r UserRepository) GetTeacherInfo(ctx context.Context, querier interfaces.IQuerier,
	teacherId pgtype.Int4) (teacher dao.Teacher, err error) {
	const query = `
		SELECT CONCAT_WS(' ', last_name, first_name, middle_name), position_name, image_link, email, phone
		FROM users WHERE id = $1;
	`
	if err = querier.QueryRow(ctx, query, teacherId).Scan(&teacher.Name, &teacher.PositionName, &teacher.ImageLink,
		&teacher.Email, &teacher.Phone); err != nil {
		return dao.Teacher{}, errors.Wrap(err, "Scan")
	}
	return
}

func (r UserRepository) GetStudentInfo(ctx context.Context, querier interfaces.IQuerier,
	studentId pgtype.Int4) (student dao.Student, err error) {
	const query = `
		SELECT CONCAT_WS(' ', last_name, first_name, middle_name) fio, image_link, email, phone
		FROM users WHERE id = $1;
	`
	if err = querier.QueryRow(ctx, query, studentId).Scan(&student.Name, &student.ImageLink,
		&student.Email, &student.Phone); err != nil {
		return dao.Student{}, errors.Wrap(err, "Scan")
	}
	return
}
