package repositories

import (
	"context"
	errs "diploma/internal/errors"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"time"
)

type TaskRepository struct {
	FilePath string
}

func (r TaskRepository) InsertTask(ctx context.Context, querier interfaces.IQuerier, task dao.Task) (id pgtype.Int4, err error) {
	const query = `
		INSERT INTO tasks (title, subtitle, end_date, description, teacher_id, group_id, subject_id)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
		RETURNING id;
	`
	if err = querier.QueryRow(ctx, query, task.Title, task.Subtitle, task.EndDate,
		task.Description, task.TeacherId, task.GroupId, task.SubjectId).Scan(&id); err != nil {
		return pgtype.Int4{}, errors.Wrap(err, "Scan")
	}
	return
}

func (r TaskRepository) TakeAnswers(ctx context.Context, querier interfaces.IQuerier,
	studentId pgtype.Int4) (tasks []dao.TaskToStudent, err error) {
	const query = `
		SELECT t.title, t.subtitle, t.start_date, t.end_date, s.status_en, a.id, t.description,
        	CONCAT_WS(' ', u.last_name, u.first_name, u.middle_name) fio, u.position_name, u.image_link,
        	u.email, u.phone, a.status_id, t.id, a.comment
        FROM answers a JOIN tasks t ON a.task_id = t.id
                      JOIN statuses s ON a.status_id = s.id
                      JOIN users u ON t.teacher_id = u.id
        WHERE a.student_id = $1;
	`
	rows, err := querier.Query(ctx, query, studentId)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	tasks = make([]dao.TaskToStudent, 0, 10)
	for rows.Next() {
		var task dao.TaskToStudent
		if err = rows.Scan(&task.Title, &task.Subtitle, &task.StartDate, &task.EndDate, &task.Status,
			&task.Id, &task.Description, &task.Teacher.Name, &task.Teacher.PositionName, &task.Teacher.ImageLink,
			&task.Teacher.Email, &task.Teacher.Phone, &task.StatusId, &task.TaskId, &task.Comment); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		tasks = append(tasks, task)
	}
	return
}

func (r TaskRepository) TakeFiles(ctx context.Context, querier interfaces.IQuerier,
	taskId pgtype.Int4) (files []pgtype.Text, err error) {
	const query = `
		SELECT file_link FROM files_tasks
		WHERE task_id = $1
	`
	var rows pgx.Rows
	if rows, err = querier.Query(ctx, query, taskId); err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	files = make([]pgtype.Text, 0, 3)
	for rows.Next() {
		var file pgtype.Text
		if err = rows.Scan(&file); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		files = append(files, file)
	}
	return
}

func (r TaskRepository) TakeOptions(ctx context.Context, querier interfaces.IQuerier,
	statusId pgtype.Int4) (options []dao.Option, err error) {
	const query = `
		SELECT status_en, status_ru
		FROM statuses
		WHERE id = $1;
	`
	nextStatus := statusId.Int + 1
	prevStatus := statusId.Int - 1
	options = make([]dao.Option, 0, 2)
	if prevStatus != 0 {
		var option dao.Option
		if err = querier.QueryRow(ctx, query, prevStatus).Scan(&option.Action, &option.Text); err != nil {
			return nil, errors.Wrap(err, "scan prev status")
		}
		options = append(options, option)
	}
	if nextStatus != 5 {
		var option dao.Option
		if err = querier.QueryRow(ctx, query, nextStatus).Scan(&option.Action, &option.Text); err != nil {
			return nil, errors.Wrap(err, "scan next status")
		}
		options = append(options, option)
	}
	return
}

func (r TaskRepository) InsertFileName(ctx context.Context, querier interfaces.IQuerier, taskId pgtype.Int4,
	filename pgtype.Text) (fileLink string, err error) {
	const query = `
		INSERT INTO files_tasks (task_id, file_link, file_name)
		VALUES ($1, $2, $3);
	`
	fileLink = r.FilePath + filename.String
	var fileLinkDb pgtype.Text
	if err = fileLinkDb.Set(fileLink); err != nil {
		return "", errors.Wrap(err, "Set")
	}
	if _, err = querier.Exec(ctx, query, taskId, fileLinkDb, filename); err != nil {
		return "", errors.Wrap(err, "Exec")
	}
	return
}

func (r TaskRepository) TakeJournalDates(ctx context.Context, querier interfaces.IQuerier,
	teacherId, groupId, subjectId pgtype.Int4) (dates []pgtype.Date, err error) {
	const query = `
		SELECT DATE(start_date) FROM tasks
		WHERE group_id = $1 AND subject_id = $2 AND teacher_id = $3
		ORDER BY start_date;
	`
	rows, err := querier.Query(ctx, query, groupId, subjectId, teacherId)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	dates = make([]pgtype.Date, 0, 10)
	for rows.Next() {
		var date pgtype.Date
		if err = rows.Scan(&date); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		dates = append(dates, date)
	}
	return
}

func (r TaskRepository) TakeAnswersForReview(ctx context.Context, querier interfaces.IQuerier,
	teacherId, subjectId, groupId pgtype.Int4) (tasks []dao.TaskToStudent, err error) {
	const query = `
		SELECT t.title, t.subtitle, t.start_date, t.end_date, s.status_en, a.id, t.description,
        	CONCAT_WS(' ', u.last_name, u.first_name, u.middle_name) fio, u.image_link,
        	u.email, u.phone, a.status_id, t.id, a.comment
        FROM answers a JOIN tasks t ON a.task_id = t.id
                      JOIN statuses s ON a.status_id = s.id
                      JOIN users u ON a.student_id = u.id
        WHERE t.teacher_id = $1 AND t.group_id = $2 AND t.subject_id = $3 AND a.status_id=3;
	`
	const queryAll = `
		SELECT t.title, t.subtitle, t.start_date, t.end_date, s.status_en, a.id, t.description,
        	CONCAT_WS(' ', u.last_name, u.first_name, u.middle_name) fio, u.image_link,
        	u.email, u.phone, a.status_id, t.id, a.comment
        FROM answers a JOIN tasks t ON a.task_id = t.id
                      JOIN statuses s ON a.status_id = s.id
                      JOIN users u ON a.student_id = u.id
        WHERE t.teacher_id = $1 AND a.status_id=3;
	`
	var rows pgx.Rows
	if groupId.Int == -1 && subjectId.Int == -1 {
		rows, err = querier.Query(ctx, queryAll, teacherId)
	} else {
		rows, err = querier.Query(ctx, query, teacherId, groupId, subjectId)
	}
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	tasks = make([]dao.TaskToStudent, 0, 10)
	for rows.Next() {
		var task dao.TaskToStudent
		if err = rows.Scan(&task.Title, &task.Subtitle, &task.StartDate, &task.EndDate, &task.Status,
			&task.Id, &task.Description, &task.Teacher.Name, &task.Teacher.ImageLink,
			&task.Teacher.Email, &task.Teacher.Phone, &task.StatusId, &task.TaskId, &task.Comment); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		tasks = append(tasks, task)
	}
	return
}

func (r TaskRepository) CheckExist(ctx context.Context, querier interfaces.IQuerier, task dao.Task) (err error) {
	const query = `
		SELECT id
		FROM tasks
		WHERE DATE(start_date) = $1 AND subject_id = $2 AND group_id = $3;
	`
	var today pgtype.Date
	if err = today.Set(time.Now()); err != nil {
		return errors.Wrap(err, "Set")
	}
	var taskId pgtype.Int4
	if err = querier.QueryRow(ctx, query, today, task.SubjectId, task.GroupId).Scan(&taskId); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil
		}
		return errors.Wrap(err, "QueryRow")
	}
	return errs.ErrTaskExist
}
