package repositories

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type SubjectRepository struct {
}

func (r *SubjectRepository) SelectSubjectsByTeacherId(ctx context.Context, querier interfaces.IQuerier,
	TeacherId pgtype.Int4) (subjects []dao.KeyValue, err error) {
	const query = `
		SELECT s.id, s.name
		FROM subjects s JOIN teachers_subjects ts on s.id = ts.subject_id
		WHERE ts.teacher_id = $1;
	`
	rows, err := querier.Query(ctx, query, TeacherId)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	subjects = make([]dao.KeyValue, 0, 5)
	for rows.Next() {
		var subject dao.KeyValue
		if err = rows.Scan(&subject.Id, &subject.Name); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		subjects = append(subjects, subject)
	}
	return
}

func (r *SubjectRepository) GetSubjectName(ctx context.Context, querier interfaces.IQuerier,
	subjectId pgtype.Int4) (subjectName pgtype.Text, err error) {
	const query = `
		SELECT name
		FROM subjects
		WHERE id = $1;
	`
	if err = querier.QueryRow(ctx, query, subjectId).Scan(&subjectName); err != nil {
		return pgtype.Text{}, errors.Wrap(err, "Scan")
	}
	return
}
