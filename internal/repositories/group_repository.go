package repositories

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type GroupRepository struct {
}

func (r *GroupRepository) GetGroupById(ctx context.Context, querier interfaces.IQuerier,
	id pgtype.Int4) (group dao.Group, err error) {
	const query = `
		SELECT group_name, faculty, direction FROM study_groups
		WHERE id = $1;
	`
	if err = querier.QueryRow(ctx, query, id).Scan(&group.GroupName, &group.Faculty, &group.Direction); err != nil {
		return dao.Group{}, errors.Wrap(err, "GetGroupById")
	}
	return
}

func (r *GroupRepository) GetStudentsList(ctx context.Context, querier interfaces.IQuerier,
	id pgtype.Int4) (students []dao.Student, err error) {
	const query = `
		SELECT CONCAT_WS(' ', last_name, first_name, middle_name), phone, email, image_link
		FROM users
		WHERE group_id = $1;
	`
	rows, err := querier.Query(ctx, query, id)
	if err != nil {
		return nil, errors.Wrap(err, "GetStudentsList")
	}
	defer rows.Close()
	students = make([]dao.Student, 0, 30)
	for rows.Next() {
		var student dao.Student
		if err = rows.Scan(&student.Name, &student.Phone, &student.Email, &student.ImageLink); err != nil {
			return nil, errors.Wrap(err, "GetStudentsList")
		}
		students = append(students, student)
	}
	return
}

func (r *GroupRepository) GetStudentsId(ctx context.Context, querier interfaces.IQuerier,
	id pgtype.Int4) (studentIds []pgtype.Int4, err error) {
	const query = `
		SELECT id
		FROM users
		WHERE group_id = $1;
	`
	rows, err := querier.Query(ctx, query, id)
	if err != nil {
		return nil, errors.Wrap(err, "GetStudentsId")
	}
	defer rows.Close()
	studentIds = make([]pgtype.Int4, 0, 30)
	for rows.Next() {
		var id pgtype.Int4
		if err = rows.Scan(&id); err != nil {
			return nil, errors.Wrap(err, "GetStudentsId")
		}
		studentIds = append(studentIds, id)
	}
	return
}

func (r *GroupRepository) SelectGroupsByTeacherId(ctx context.Context, querier interfaces.IQuerier,
	teacherId pgtype.Int4) (groups []dao.KeyValue, err error) {
	const query = `
		SELECT DISTINCT t.group_id, sg.group_name
		FROM tasks t
		JOIN study_groups sg ON t.group_id = sg.id
		WHERE teacher_id = $1;
	`
	rows, err := querier.Query(ctx, query, teacherId)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}
	groups = make([]dao.KeyValue, 0, 6)
	for rows.Next() {
		var group dao.KeyValue
		if err = rows.Scan(&group.Id, &group.Name); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}
		groups = append(groups, group)
	}
	return
}
