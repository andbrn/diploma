package dto

type PostStatus struct {
	AnswerId int32  `json:"answerId"`
	Status   string `json:"status"`
	Comment  string `json:"comment"`
}
