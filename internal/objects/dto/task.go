package dto

type Task struct {
	GroupId     int32  `json:"groupId"`
	SubjectId   int32  `json:"subjectId"`
	Title       string `json:"taskTitle"`
	Subtitle    string `json:"taskSubtitle"`
	Description string `json:"taskDescription"`
	EndDate     string `json:"taskEndDate"`
}

type TaskToStudent struct {
	Title       string              `json:"title"`
	Subtitle    string              `json:"subtitle"`
	StartDate   string              `json:"startDate"`
	EndDate     string              `json:"endDate"`
	Status      string              `json:"status"`
	Id          int32               `json:"id"`
	Options     []Option            `json:"options"`
	Description string              `json:"description"`
	Person      Person              `json:"person"`
	Files       []map[string]string `json:"files"`
	Comment     string              `json:"comment"`
	Editable    bool                `json:"editable"`
	Burned      bool                `json:"burned,omitempty"`
}

type Option struct {
	Text   string `json:"text"`
	Action string `json:"action"`
}

type Person struct {
	Name     string `json:"name"`
	Position string `json:"position,omitempty"`
	Image    string `json:"image"`
	Email    string `json:"email"`
	Tel      string `json:"tel"`
}
