package dto

type Cell struct {
	FirstCell bool   `json:"firstCell,omitempty"`
	Text      string `json:"text"`
	Data      *Data  `json:"data,omitempty"`
	Status    string `json:"status,omitempty"`
	Id        int32  `json:"id,omitempty"`
	Burned    bool   `json:"burned,omitempty"`
}

type Data struct {
	Title       string              `json:"title"`
	Description string              `json:"description"`
	Person      Person              `json:"person,omitempty"`
	Id          int32               `json:"id,omitempty"`
	Subtitle    string              `json:"subtitle,omitempty"`
	StartDate   string              `json:"startDate,omitempty"`
	EndDate     string              `json:"endDate,omitempty"`
	Status      string              `json:"status,omitempty"`
	Editable    bool                `json:"editable,omitempty"`
	Files       []map[string]string `json:"files"`
	Comment     string              `json:"comment"`
	Options     []Option            `json:"options,omitempty"`
	Burned      bool                `json:"burned,omitempty"`
}
