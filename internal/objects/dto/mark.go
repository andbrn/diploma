package dto

type SetMark struct {
	AnswerId int32   `json:"answerId"`
	Mark     float32 `json:"mark"`
	Comment  string  `json:"comment"`
}
