package dto

type User struct {
	IsStudent    bool   `json:"isStudent"`
	Id           int32  `json:"id"`
	FirstName    string `json:"firstName"`
	MiddleName   string `json:"middleName,omitempty"`
	LastName     string `json:"lastName"`
	ImageLink    string `json:"imageLink,omitempty"`
	Email        string `json:"email"`
	Phone        string `json:"phone,omitempty"`
	GroupId      int32  `json:"groupId,omitempty"`
	PositionName string `json:"positionName,omitempty"`
}
