package dto

import "diploma/internal/objects/bo"

type GetJournal struct {
	GroupId   int32 `query:"groupId"`
	SubjectId int32 `query:"subjectId"`
}

func (g *GetJournal) ToBo(teacherId int) (boG bo.GetReview) {
	boG.TeacherId = int32(teacherId)
	boG.GroupId = g.GroupId
	boG.SubjectId = g.SubjectId
	return
}
