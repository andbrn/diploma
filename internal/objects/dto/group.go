package dto

type GetGroup struct {
	GroupId int32 `query:"groupId"`
}

type Group struct {
	GroupName string `json:"groupName"`
	Faculty   string `json:"faculty"`
	Direction string `json:"direction"`
}

type Student struct {
	Name      string `json:"name"`
	ImageLink string `json:"imageLink"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}
