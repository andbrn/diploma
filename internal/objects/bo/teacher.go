package bo

type Teacher struct {
	Name         string
	ImageLink    string
	Email        string
	Phone        string
	PositionName string
}
