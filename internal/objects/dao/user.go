package dao

import (
	"diploma/internal/objects/dto"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type User struct {
	IsStudent    pgtype.Bool
	Id           pgtype.Int4
	FirstName    pgtype.Text
	MiddleName   pgtype.Text
	LastName     pgtype.Text
	ImageLink    pgtype.Text
	Email        pgtype.Text
	Phone        pgtype.Text
	GroupId      pgtype.Int4
	PositionName pgtype.Text
}

func (u User) ToDto() (user dto.User, err error) {

	if err = u.IsStudent.AssignTo(&user.IsStudent); err != nil {
		return dto.User{}, errors.Wrap(err, "AssignTo IsStudent")
	}
	if err = u.Id.AssignTo(&user.Id); err != nil {
		return dto.User{}, errors.Wrap(err, "AssignTo Id")
	}
	if err = u.FirstName.AssignTo(&user.FirstName); err != nil {
		return dto.User{}, errors.Wrap(err, "AssignTo FirstName")
	}
	if u.MiddleName.Status != pgtype.Null {
		if err = u.MiddleName.AssignTo(&user.MiddleName); err != nil {
			return dto.User{}, errors.Wrap(err, "AssignTo MiddleName")
		}
	}
	if err = u.LastName.AssignTo(&user.LastName); err != nil {
		return dto.User{}, errors.Wrap(err, "AssignTo LastName")
	}
	if u.ImageLink.Status != pgtype.Null {
		if err = u.ImageLink.AssignTo(&user.ImageLink); err != nil {
			return dto.User{}, errors.Wrap(err, "AssignTo ImageLink")
		}
	}
	if err = u.Email.AssignTo(&user.Email); err != nil {
		return dto.User{}, errors.Wrap(err, "AssignTo Email")
	}
	if u.Phone.Status != pgtype.Null {
		if err = u.Phone.AssignTo(&user.Phone); err != nil {
			return dto.User{}, errors.Wrap(err, "AssignTo Phone")
		}
	}
	if u.GroupId.Status != pgtype.Null {
		if err = u.GroupId.AssignTo(&user.GroupId); err != nil {
			return dto.User{}, errors.Wrap(err, "AssignTo GroupId")
		}
	}
	if u.PositionName.Status != pgtype.Null {
		if err = u.PositionName.AssignTo(&user.PositionName); err != nil {
			return dto.User{}, errors.Wrap(err, "AssignTo PositionName")
		}
	}
	return
}

type Teacher struct {
	Name         pgtype.Text
	ImageLink    pgtype.Text
	Email        pgtype.Text
	Phone        pgtype.Text
	PositionName pgtype.Text
}

func (t Teacher) ToDto() (p dto.Person, err error) {
	if err = t.Name.AssignTo(&p.Name); err != nil {
		return dto.Person{}, errors.Wrap(err, "AssignTo Name")
	}
	if err = t.PositionName.AssignTo(&p.Position); err != nil {
		return dto.Person{}, errors.Wrap(err, "AssignTo Position")
	}
	if t.Phone.Status != pgtype.Null {
		if err = t.Phone.AssignTo(&p.Tel); err != nil {
			return dto.Person{}, errors.Wrap(err, "AssignTo Tel")
		}
	}
	if err = t.Email.AssignTo(&p.Email); err != nil {
		return dto.Person{}, errors.Wrap(err, "AssignTo Email")
	}
	if t.ImageLink.Status != pgtype.Null {
		if err = t.ImageLink.AssignTo(&p.Image); err != nil {
			return dto.Person{}, errors.Wrap(err, "AssignTo Image")
		}
	}
	return
}
