package dao

import (
	"diploma/internal/objects/dto"
	"fmt"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
	"time"
)

type Task struct {
	GroupId     pgtype.Int4
	SubjectId   pgtype.Int4
	Title       pgtype.Text
	Subtitle    pgtype.Text
	Description pgtype.Text
	EndDate     pgtype.Timestamptz
	TeacherId   pgtype.Int4
}

func (t *Task) FillFromDto(task dto.Task, teacherId int32) (err error) {
	if err = t.GroupId.Set(task.GroupId); err != nil {
		return errors.Wrap(err, "Set GroupId")
	}
	if err = t.SubjectId.Set(task.SubjectId); err != nil {
		return errors.Wrap(err, "Set SubjectName")
	}
	if err = t.Title.Set(task.Title); err != nil {
		return errors.Wrap(err, "Set Title")
	}
	if err = t.Subtitle.Set(task.Subtitle); err != nil {
		return errors.Wrap(err, "Set Subtitle")
	}
	if err = t.Description.Set(task.Description); err != nil {
		return errors.Wrap(err, "Set Description")
	}
	date, err := time.Parse("2006-01-02", task.EndDate)
	if err != nil {
		return errors.Wrap(err, "Parse EndDate")
	}
	if err = t.EndDate.Set(date); err != nil {
		return errors.Wrap(err, "Set EndDate")
	}
	if err = t.TeacherId.Set(teacherId); err != nil {
		return errors.Wrap(err, "Set TeacherId")
	}
	return
}

type TaskToStudent struct {
	Id           pgtype.Int4
	TaskId       pgtype.Int4
	Title        pgtype.Text
	Subtitle     pgtype.Text
	Description  pgtype.Text
	EndDate      pgtype.Timestamptz
	StartDate    pgtype.Timestamptz
	Status       pgtype.Text
	Teacher      Teacher
	Options      []Option
	StatusId     pgtype.Int4
	StudentFiles []pgtype.Text
	TeacherFiles []pgtype.Text
	Comment      pgtype.Text
}

func (t *TaskToStudent) ToDto() (taskDto dto.TaskToStudent, err error) {
	taskDto.Editable = true
	if err = t.Id.AssignTo(&taskDto.Id); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Id")
	}
	if err = t.Title.AssignTo(&taskDto.Title); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Title")
	}
	if err = t.Subtitle.AssignTo(&taskDto.Subtitle); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Subtitle")
	}
	if t.Description.Status != pgtype.Null {
		if err = t.Description.AssignTo(&taskDto.Description); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Description")
		}
	}
	if t.Comment.Status != pgtype.Null {
		if err = t.Comment.AssignTo(&taskDto.Comment); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Comment")
		}
	}
	if err = t.Status.AssignTo(&taskDto.Status); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Status")
	}
	if t.Teacher.Phone.Status != pgtype.Null {
		if err = t.Teacher.Phone.AssignTo(&taskDto.Person.Tel); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Tel")
		}
	}
	if err = t.Teacher.Name.AssignTo(&taskDto.Person.Name); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Name")
	}
	if t.Teacher.ImageLink.Status != pgtype.Null {
		if err = t.Teacher.ImageLink.AssignTo(&taskDto.Person.Image); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Image")
		}
	}
	if t.Teacher.PositionName.Status != pgtype.Null && t.Teacher.PositionName.String != "" {
		if err = t.Teacher.PositionName.AssignTo(&taskDto.Person.Position); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Position")
		}
	}
	if err = t.Teacher.Email.AssignTo(&taskDto.Person.Email); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Email")
	}

	taskDto.Options = make([]dto.Option, 0, len(t.Options))
	for _, option := range t.Options {
		optionDto := dto.Option{}
		if err = option.Action.AssignTo(&optionDto.Action); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Action")
		}
		if err = option.Text.AssignTo(&optionDto.Text); err != nil {
			return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo Text")
		}
		taskDto.Options = append(taskDto.Options, optionDto)
	}

	taskDto.Files = make([]map[string]string, 0, len(t.StudentFiles)+len(t.TeacherFiles))

	for i, file := range t.StudentFiles {
		taskDto.Files = append(taskDto.Files, map[string]string{
			"name": fmt.Sprintf("student-%d", i),
			"href": file.String,
		})
	}
	for i, file := range t.TeacherFiles {
		taskDto.Files = append(taskDto.Files, map[string]string{
			"name": fmt.Sprintf("teacher-%d", i),
			"href": file.String,
		})
	}

	var endTime, startTime time.Time
	if err = t.EndDate.AssignTo(&endTime); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo endTime")
	}
	if err = t.StartDate.AssignTo(&startTime); err != nil {
		return dto.TaskToStudent{}, errors.Wrap(err, "AssignTo startTime")
	}
	taskDto.EndDate = endTime.Format("02.01.2006")
	taskDto.StartDate = startTime.Format("02.01.2006")
	if time.Since(t.EndDate.Time).Microseconds() > 0 && taskDto.Status != "done" {
		taskDto.Burned = true
	}
	return
}

type Option struct {
	Text   pgtype.Text
	Action pgtype.Text
}
