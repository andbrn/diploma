package dao

import (
	"github.com/jackc/pgtype"
)

type CellAnswer struct {
	Text        pgtype.Text
	Status      pgtype.Text
	Id          pgtype.Int4
	TaskId      pgtype.Int4
	Title       pgtype.Text
	SubTittle   pgtype.Text
	StartDate   pgtype.Timestamptz
	EndDate     pgtype.Timestamptz
	Description pgtype.Text
	Comment     pgtype.Text
	PersonId    pgtype.Int4
	StatusId    pgtype.Int4
	SubjectId   pgtype.Int4
}
