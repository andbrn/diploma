package dao

import (
	"diploma/internal/objects/dto"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type Group struct {
	GroupName pgtype.Text
	Faculty   pgtype.Text
	Direction pgtype.Text
}

func (g Group) ToDto() (group dto.Group, err error) {
	if err = g.GroupName.AssignTo(&group.GroupName); err != nil {
		return dto.Group{}, errors.Wrap(err, "AssignTo GroupName")
	}
	if err = g.Direction.AssignTo(&group.Direction); err != nil {
		return dto.Group{}, errors.Wrap(err, "AssignTo Direction")
	}
	if err = g.Faculty.AssignTo(&group.Faculty); err != nil {
		return dto.Group{}, errors.Wrap(err, "AssignTo Faculty")
	}
	return
}

type Student struct {
	Name      pgtype.Text
	ImageLink pgtype.Text
	Email     pgtype.Text
	Phone     pgtype.Text
}

func StudentsToDto(daoStudents []Student) (students []dto.Student, err error) {
	students = make([]dto.Student, 0, len(daoStudents))
	for _, daoStudent := range daoStudents {
		var student dto.Student
		if err = daoStudent.Name.AssignTo(&student.Name); err != nil {
			return nil, errors.Wrap(err, "AssignTo Name")
		}
		if daoStudent.ImageLink.Status != pgtype.Null {
			if err = daoStudent.ImageLink.AssignTo(&student.ImageLink); err != nil {
				return nil, errors.Wrap(err, "AssignTo ImageLink")
			}
		}
		if err = daoStudent.Email.AssignTo(&student.Email); err != nil {
			return nil, errors.Wrap(err, "AssignTo Email")
		}
		if daoStudent.Phone.Status != pgtype.Null {
			if err = daoStudent.Phone.AssignTo(&student.Phone); err != nil {
				return nil, errors.Wrap(err, "AssignTo Phone")
			}
		}
		students = append(students, student)
	}
	return
}
