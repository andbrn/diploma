package dao

import (
	"diploma/internal/objects/dto"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type KeyValue struct {
	Id   pgtype.Int4
	Name pgtype.Text
}

func (k *KeyValue) ToDto() (dtoKeyValue dto.KeyValue, err error) {
	if err = k.Name.AssignTo(&dtoKeyValue.Name); err != nil {
		return dto.KeyValue{}, errors.Wrap(err, "AssignTo Name")
	}
	if err = k.Id.AssignTo(&dtoKeyValue.Id); err != nil {
		return dto.KeyValue{}, errors.Wrap(err, "AssignTo Id")
	}
	return
}
