package dao

import "github.com/jackc/pgtype"

type Answer struct {
	Id        pgtype.Int4
	TaskId    pgtype.Int4
	StudentId pgtype.Int4
	StatusId  pgtype.Int4
	Comment   pgtype.Text
	Mark      pgtype.Numeric
}
