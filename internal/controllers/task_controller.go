package controllers

import (
	errs "diploma/internal/errors"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dto"
	"github.com/labstack/echo"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
)

type TaskController struct {
	Log           *zap.SugaredLogger
	TaskService   interfaces.ITaskService
	AnswerService interfaces.IAnswerService
}

func (c *TaskController) NewTask(ctx echo.Context) (err error) {
	var (
		request   dto.Task
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
	)
	if err = ctx.Bind(&request); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"error": err.Error(),
		})
	}
	teacherId, err := strconv.Atoi(strings.Split(ctx.Request().Header.Get("Authorization"), " ")[0])
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"error": err.Error(),
		})
	}
	taskId, err := c.TaskService.AddTask(request, int32(teacherId))
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		if errors.Is(err, errs.ErrTaskExist) {
			return ctx.JSON(http.StatusConflict, echo.Map{
				"error": "Сегодня вы уже добавляли задание в эту группу по этому предмету",
			})
		}
		return ctx.JSON(http.StatusInternalServerError, echo.Map{
			"error": err.Error(),
		})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"taskId": taskId,
	})
}

func (c *TaskController) UploadFiles(ctx echo.Context) (err error) {
	var (
		formFile  *multipart.FileHeader
		formData  multipart.File
		taskId    int
		fileLink  string
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
	)
	if taskId, err = strconv.Atoi(ctx.Param("taskId")); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.String(http.StatusBadRequest, "")
	}
	isTask := ctx.QueryParam("isTask")
	fileLinks := make([]string, 0, 3)
	for i := 1; ; i++ {
		if err = func() error {
			if formFile, err = ctx.FormFile("file-" + strconv.Itoa(i)); err != nil {
				return errors.New("no more files")
			}
			if formData, err = formFile.Open(); err != nil {
				return errors.Wrap(err, "Open")
			}
			defer formData.Close()
			filenameParts := strings.Split(formFile.Filename, ".")
			extension := filenameParts[len(filenameParts)-1]
			if isTask != "true" {
				if fileLink, err = c.TaskService.UploadFileToAnswer(formData, int32(taskId), extension); err != nil {
					return err
				}
			} else {
				if fileLink, err = c.TaskService.UploadFileToTask(formData, int32(taskId), extension); err != nil {
					return err
				}
			}
			fileLinks = append(fileLinks, fileLink)
			return nil
		}(); err != nil {
			if err.Error() == "no more files" {
				break
			}
			c.Log.Error(err.Error(), logFields)
			return ctx.String(http.StatusInternalServerError, "")
		}
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"links": fileLinks,
	})
}

func (c *TaskController) TasksToStudent(ctx echo.Context) (err error) {
	var (
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
		tasks     []dto.TaskToStudent
	)
	studentId, err := strconv.Atoi(strings.Split(ctx.Request().Header.Get("Authorization"), " ")[0])
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if tasks, err = c.TaskService.TakeTasks(int32(studentId)); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"taskList": tasks,
	})
}

func (c *TaskController) Journal(ctx echo.Context) (err error) {
	var (
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
		table     [][]dto.Cell
		params    dto.GetJournal
	)
	if err = ctx.Bind(&params); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	teacherId, err := strconv.Atoi(strings.Split(ctx.Request().Header.Get("Authorization"), " ")[0])
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if table, err = c.AnswerService.TakeJournal(int32(teacherId), params.GroupId, params.SubjectId); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"table": table,
	})
}

func (c *TaskController) Diary(ctx echo.Context) (err error) {
	var (
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
		table     [][]dto.Cell
	)

	studentId, err := strconv.Atoi(strings.Split(ctx.Request().Header.Get("Authorization"), " ")[0])
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if table, err = c.AnswerService.TakeDiary(int32(studentId)); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"table": table,
	})
}

func (c *TaskController) ChangeStatus(ctx echo.Context) (err error) {
	var (
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
		params    dto.PostStatus
	)
	if err = ctx.Bind(&params); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if err = c.AnswerService.UpdateStatus(params.Status, params.AnswerId, params.Comment); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{})
}

func (c *TaskController) SetMark(ctx echo.Context) (err error) {
	var (
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
		params    dto.SetMark
	)
	if err = ctx.Bind(&params); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if err = c.AnswerService.SetMark(params.Mark, params.Comment, params.AnswerId); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{})
}

func (c *TaskController) Review(ctx echo.Context) (err error) {
	var (
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
		tasks     []dto.TaskToStudent
		params    dto.GetJournal
	)
	teacherId, err := strconv.Atoi(strings.Split(ctx.Request().Header.Get("Authorization"), " ")[0])
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if err = ctx.Bind(&params); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{})
	}
	if tasks, err = c.TaskService.TakeTasksForReview(params.ToBo(teacherId)); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"taskList": tasks,
	})
}
