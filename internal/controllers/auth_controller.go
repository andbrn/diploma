package controllers

import (
	"diploma/internal/interfaces"
	"diploma/internal/objects/dto"
	"github.com/labstack/echo"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"strings"
)

type AuthController struct {
	Log            *zap.SugaredLogger
	AuthService    interfaces.IAuthService
	GroupService   interfaces.IGroupService
	SubjectService interfaces.ISubjectService
}

func (c *AuthController) ProcessAuth(ctx echo.Context) (err error) {
	var (
		request          dto.Auth
		user             dto.User
		groups, subjects []dto.KeyValue
		token            string
		logFields        = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
	)

	if err = ctx.Bind(&request); err != nil {
		c.Log.Warnw(err.Error(), logFields...)
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"error": err.Error(),
		})
	}
	logFields = append(logFields, "request", request)

	if user, token, err = c.AuthService.ProcessAuth(request); err != nil {
		c.Log.Errorw(err.Error(), logFields...)
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"error": err.Error(),
		})
	}
	if !user.IsStudent {
		if groups, err = c.GroupService.GetGroupsByTeacherId(user.Id); err != nil {
			c.Log.Errorw(err.Error(), logFields...)
			return ctx.JSON(http.StatusInternalServerError, echo.Map{})
		}
		if subjects, err = c.SubjectService.GetSubjectsByTeacherId(user.Id); err != nil {
			c.Log.Errorw(err.Error(), logFields...)
			return ctx.JSON(http.StatusInternalServerError, echo.Map{})
		}
		c.Log.Info(logFields)
		return ctx.JSON(http.StatusOK, echo.Map{
			"token":    token,
			"user":     user,
			"subjects": subjects,
			"groups":   groups,
		})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"token": token,
		"user":  user,
	})
}

func (c *AuthController) TakeInfo(ctx echo.Context) (err error) {
	var (
		userId           int
		user             dto.User
		groups, subjects []dto.KeyValue
		logFields        = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
	)

	userId, err = strconv.Atoi(strings.Split(ctx.Request().Header.Get("Authorization"), " ")[0])

	if user, err = c.AuthService.UserInfo(int32(userId)); err != nil {
		c.Log.Errorw(err.Error(), logFields...)
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"error": err.Error(),
		})
	}

	if !user.IsStudent {
		if groups, err = c.GroupService.GetGroupsByTeacherId(user.Id); err != nil {
			c.Log.Errorw(err.Error(), logFields...)
			return ctx.JSON(http.StatusInternalServerError, echo.Map{})
		}
		if subjects, err = c.SubjectService.GetSubjectsByTeacherId(user.Id); err != nil {
			c.Log.Errorw(err.Error(), logFields...)
			return ctx.JSON(http.StatusInternalServerError, echo.Map{})
		}
		c.Log.Info(logFields)
		return ctx.JSON(http.StatusOK, echo.Map{
			"user":     user,
			"subjects": subjects,
			"groups":   groups,
		})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"user": user,
	})
}
