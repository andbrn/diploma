package controllers

import (
	"diploma/internal/interfaces"
	"diploma/internal/objects/dto"
	"github.com/labstack/echo"
	"go.uber.org/zap"
	"net/http"
)

type GroupController struct {
	Log          *zap.SugaredLogger
	GroupService interfaces.IGroupService
}

func (c *GroupController) GroupList(ctx echo.Context) (err error) {
	var (
		request   dto.GetGroup
		logFields = []interface{}{"path", ctx.Path(), "method", ctx.Request().Method}
	)
	if err = ctx.Bind(&request); err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusBadRequest, echo.Map{
			"error": err.Error(),
		})
	}
	group, students, err := c.GroupService.GetGroup(request.GroupId)
	if err != nil {
		c.Log.Error(err.Error(), logFields)
		return ctx.JSON(http.StatusInternalServerError, echo.Map{
			"error": err.Error(),
		})
	}
	c.Log.Info(logFields)
	return ctx.JSON(http.StatusOK, echo.Map{
		"group":    group,
		"students": students,
	})
}
