package errs

import "github.com/pkg/errors"

var ErrTaskExist = errors.New("task already exists")
