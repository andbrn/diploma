package interfaces

import (
	"context"
	"diploma/internal/objects/dao"
	"github.com/jackc/pgtype"
)

type IAuthRepository interface {
	Auth(ctx context.Context, querier IQuerier, email string, password string) (userId pgtype.Int4, err error)
	GenerateToken(ctx context.Context, querier IQuerier, userId pgtype.Int4) (token pgtype.UUID, err error)
	CheckAuth(ctx context.Context, querier IQuerier, userId pgtype.Int4, token pgtype.UUID) (sc int, err error)
	GetInfoByEmail(ctx context.Context, querier IQuerier, email pgtype.Text) (user dao.User, err error)
	IsStudent(ctx context.Context, querier IQuerier, userId pgtype.Int4) (isStudent bool, err error)
	GetInfoById(ctx context.Context, querier IQuerier, id pgtype.Int4) (user dao.User, err error)
}

type IGroupRepository interface {
	GetGroupById(ctx context.Context, querier IQuerier, id pgtype.Int4) (group dao.Group, err error)
	GetStudentsList(ctx context.Context, querier IQuerier, id pgtype.Int4) (students []dao.Student, err error)
	GetStudentsId(ctx context.Context, querier IQuerier, id pgtype.Int4) (studentIds []pgtype.Int4, err error)
	SelectGroupsByTeacherId(ctx context.Context, querier IQuerier,
		teacherId pgtype.Int4) (groups []dao.KeyValue, err error)
}

type ITaskRepository interface {
	InsertTask(ctx context.Context, querier IQuerier, task dao.Task) (id pgtype.Int4, err error)
	TakeAnswers(ctx context.Context, querier IQuerier, studentId pgtype.Int4) (tasks []dao.TaskToStudent, err error)
	TakeOptions(ctx context.Context, querier IQuerier, statusId pgtype.Int4) (options []dao.Option, err error)
	InsertFileName(ctx context.Context, querier IQuerier, taskId pgtype.Int4,
		filename pgtype.Text) (fileLink string, err error)
	TakeFiles(ctx context.Context, querier IQuerier, taskId pgtype.Int4) (files []pgtype.Text, err error)
	TakeJournalDates(ctx context.Context, querier IQuerier, teacherId, groupId,
		subjectId pgtype.Int4) (dates []pgtype.Date, err error)
	TakeAnswersForReview(ctx context.Context, querier IQuerier, teacherId, subjectId,
		groupId pgtype.Int4) (tasks []dao.TaskToStudent, err error)
	CheckExist(ctx context.Context, querier IQuerier, task dao.Task) (err error)
}

type IUserRepository interface {
	GetTeacherInfo(ctx context.Context, querier IQuerier, teacherId pgtype.Int4) (teacher dao.Teacher, err error)
	GetStudentInfo(ctx context.Context, querier IQuerier, studentId pgtype.Int4) (student dao.Student, err error)
}

type IAnswerRepository interface {
	AddAnswers(ctx context.Context, querier IQuerier, studentIds []pgtype.Int4, taskId pgtype.Int4) (err error)
	SelectAnswersByGroupAndSubject(ctx context.Context, querier IQuerier, teacherId, groupId,
		subjectId pgtype.Int4) (answers []dao.CellAnswer, err error)
	ChangeStatus(ctx context.Context, querier IQuerier, status, comment pgtype.Text, answerId pgtype.Int4) (err error)
	SetMark(ctx context.Context, querier IQuerier, mark pgtype.Numeric, comment pgtype.Text,
		answerId pgtype.Int4) (err error)
	TakeFiles(ctx context.Context, querier IQuerier, answerId pgtype.Int4) (files []pgtype.Text, err error)
	InsertFileName(ctx context.Context, querier IQuerier, answerId pgtype.Int4,
		filename pgtype.Text) (fileLink string, err error)
	TakeDiaryDates(ctx context.Context, querier IQuerier, studentId pgtype.Int4) (dates []pgtype.Date, err error)
	SelectAnswersByStudent(ctx context.Context, querier IQuerier, studentId pgtype.Int4) ([]dao.CellAnswer, error)
}

type ISubjectRepository interface {
	SelectSubjectsByTeacherId(ctx context.Context, querier IQuerier,
		TeacherId pgtype.Int4) (subjects []dao.KeyValue, err error)
	GetSubjectName(ctx context.Context, querier IQuerier,
		subjectId pgtype.Int4) (subjectName pgtype.Text, err error)
}
