package interfaces

import (
	"context"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type IDBHandler interface {
	ApplyMigration(databaseURL, migrationPath string) (result string, err error)
	AcquireConn(ctx context.Context) (*pgxpool.Conn, error)
	StartTransaction(ctx context.Context) (tx pgx.Tx, err error)
	FinishTransaction(ctx context.Context, tx pgx.Tx, err error) error
	Close()
}

type IQuerier interface {
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, query string, args ...interface{}) pgx.Row
	Exec(ctx context.Context, sql string, arguments ...interface{}) (commandTag pgconn.CommandTag, err error)
	SendBatch(ctx context.Context, b *pgx.Batch) pgx.BatchResults
}
