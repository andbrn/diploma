package interfaces

import (
	"diploma/internal/objects/bo"
	"diploma/internal/objects/dto"
	"mime/multipart"
)

type IAuthService interface {
	ProcessAuth(requestBody dto.Auth) (dto.User, string, error)
	CheckAuth(userId int32, token string) (sc int, isStudent bool, err error)
	UserInfo(userId int32) (user dto.User, err error)
}

type IGroupService interface {
	GetGroup(id int32) (group dto.Group, students []dto.Student, err error)
	GetGroupsByTeacherId(id int32) (groups []dto.KeyValue, err error)
}

type ITaskService interface {
	AddTask(task dto.Task, teacherId int32) (taskId int32, err error)
	TakeTasks(studentId int32) (tasks []dto.TaskToStudent, err error)
	UploadFileToTask(formData multipart.File, taskId int32, extension string) (fileLink string, err error)
	TakeTasksForReview(review bo.GetReview) (tasks []dto.TaskToStudent, err error)
	UploadFileToAnswer(formData multipart.File, answerId int32, extension string) (fileLink string, err error)
}

type ISubjectService interface {
	GetSubjectsByTeacherId(id int32) (subjects []dto.KeyValue, err error)
}

type IAnswerService interface {
	TakeJournal(teacherId, groupId, subjectId int32) (journal [][]dto.Cell, err error)
	TakeDiary(studentId int32) (journal [][]dto.Cell, err error)
	UpdateStatus(status string, answerId int32, comment string) (err error)
	SetMark(mark float32, comment string, answerId int32) (err error)
}
