package services

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dto"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type SubjectService struct {
	interfaces.IDBHandler
	SubjectRepository interfaces.ISubjectRepository
}

func (s SubjectService) GetSubjectsByTeacherId(id int32) (subjects []dto.KeyValue, err error) {
	var (
		teacherId pgtype.Int4
	)

	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()

	if err = teacherId.Set(id); err != nil {
		return nil, errors.Wrap(err, "AssignTo")
	}
	subjectsDb, err := s.SubjectRepository.SelectSubjectsByTeacherId(context.TODO(), conn, teacherId)
	if err != nil {
		return nil, errors.Wrap(err, "SelectSubjectsByTeacherId")
	}
	subjects = make([]dto.KeyValue, 0, 6)
	for _, subjectDb := range subjectsDb {
		subject, err := subjectDb.ToDto()
		if err != nil {
			return nil, errors.Wrap(err, "ToDto")
		}
		subjects = append(subjects, subject)
	}
	return
}
