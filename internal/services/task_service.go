package services

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/bo"
	"diploma/internal/objects/dao"
	"diploma/internal/objects/dto"
	"fmt"
	"github.com/jackc/pgtype"
	"github.com/labstack/gommon/random"
	"github.com/pkg/errors"
	"io"
	"mime/multipart"
	"os"
	"time"
)

type TaskService struct {
	interfaces.IDBHandler
	TaskRepository   interfaces.ITaskRepository
	UserRepository   interfaces.IUserRepository
	GroupRepository  interfaces.IGroupRepository
	AnswerRepository interfaces.IAnswerRepository
}

// AddTask TODO transaction
func (s *TaskService) AddTask(task dto.Task, teacherId int32) (taskId int32, err error) {
	var (
		taskDb     dao.Task
		studentIds []pgtype.Int4
	)
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return 0, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	if err = taskDb.FillFromDto(task, teacherId); err != nil {
		return 0, errors.Wrap(err, "FillFromDto")
	}

	if err = s.TaskRepository.CheckExist(context.TODO(), conn, taskDb); err != nil {
		return 0, errors.Wrap(err, "CheckExist")
	}

	taskIdDb, err := s.TaskRepository.InsertTask(context.TODO(), conn, taskDb)
	if err != nil {
		return 0, errors.Wrap(err, "InsertTask")
	}
	if err = taskIdDb.AssignTo(&taskId); err != nil {
		return 0, errors.Wrap(err, "AssignTo")
	}
	var groupId pgtype.Int4
	if err = groupId.Set(task.GroupId); err != nil {
		return 0, errors.Wrap(err, "Set")
	}
	if studentIds, err = s.GroupRepository.GetStudentsId(context.TODO(), conn, groupId); err != nil {
		return 0, errors.Wrap(err, "GetStudentsId")
	}
	fmt.Printf("%+v", studentIds[0])
	if err = s.AnswerRepository.AddAnswers(context.TODO(), conn, studentIds, taskIdDb); err != nil {
		return 0, errors.Wrap(err, "AddAnswers")
	}
	return
}

func (s *TaskService) TakeTasks(studentId int32) (tasks []dto.TaskToStudent, err error) {
	var studentIdDb pgtype.Int4
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	if err = studentIdDb.Set(studentId); err != nil {
		return nil, errors.Wrap(err, "Set")
	}
	tasksDb, err := s.TaskRepository.TakeAnswers(context.TODO(), conn, studentIdDb)
	if err != nil {
		return nil, errors.Wrap(err, "TakeStudentTasks")
	}
	if len(tasksDb) == 0 {
		return nil, nil
	}
	for i, taskDb := range tasksDb {
		options, err := s.TaskRepository.TakeOptions(context.TODO(), conn, taskDb.StatusId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeOptions")
		}
		tasksDb[i].Options = options
		tasksDb[i].TeacherFiles, err = s.TaskRepository.TakeFiles(context.TODO(), conn, taskDb.TaskId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles")
		}
		tasksDb[i].StudentFiles, err = s.AnswerRepository.TakeFiles(context.TODO(), conn, taskDb.Id)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles")
		}
	}
	tasks = make([]dto.TaskToStudent, 0, len(tasksDb))
	for _, taskDb := range tasksDb {
		var task dto.TaskToStudent
		if task, err = taskDb.ToDto(); err != nil {
			return nil, errors.Wrap(err, "ToDto")
		}
		if time.Since(taskDb.EndDate.Time) > time.Second || taskDb.Status.String == "done" {
			task.Editable = false
		}
		tasks = append(tasks, task)
	}
	return
}

func (s *TaskService) UploadFileToTask(formData multipart.File, taskId int32, extension string) (fileLink string, err error) {
	var (
		file       *os.File
		taskIdDb   pgtype.Int4
		filenameDb pgtype.Text
	)

	filename := random.String(16, "") + "." + extension
	if err = os.Chdir("/public/tasks"); err != nil {
		return "", errors.Wrap(err, "Chdir")
	}
	if file, err = os.Create(filename); err != nil {
		return "", errors.Wrap(err, "Create")
	}
	defer file.Close()
	if _, err = io.Copy(file, formData); err != nil {
		return "", errors.Wrap(err, "Copy")
	}
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return "", errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	if err = filenameDb.Set(filename); err != nil {
		return "", errors.Wrap(err, "Set filename")
	}
	if err = taskIdDb.Set(taskId); err != nil {
		return "", errors.Wrap(err, "Set taskId")
	}
	if fileLink, err = s.TaskRepository.InsertFileName(context.TODO(), conn, taskIdDb, filenameDb); err != nil {
		return "", errors.Wrap(err, "InsertFileName")
	}
	return
}

func (s *TaskService) TakeTasksForReview(review bo.GetReview) (tasks []dto.TaskToStudent, err error) {
	var (
		teacherIdDb, groupIdD, subjectIdDb pgtype.Int4
	)
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	if err = teacherIdDb.Set(review.TeacherId); err != nil {
		return nil, errors.Wrap(err, "Set")
	}
	if err = groupIdD.Set(review.GroupId); err != nil {
		return nil, errors.Wrap(err, "Set")
	}
	if err = subjectIdDb.Set(review.SubjectId); err != nil {
		return nil, errors.Wrap(err, "Set")
	}
	tasksDb, err := s.TaskRepository.TakeAnswersForReview(context.TODO(), conn, teacherIdDb, subjectIdDb, groupIdD)
	if err != nil {
		return nil, errors.Wrap(err, "TakeStudentTasks")
	}
	if len(tasksDb) == 0 {
		return nil, nil
	}
	for i, taskDb := range tasksDb {
		options, err := s.TaskRepository.TakeOptions(context.TODO(), conn, taskDb.StatusId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeOptions")
		}
		tasksDb[i].Options = options
		tasksDb[i].TeacherFiles, err = s.TaskRepository.TakeFiles(context.TODO(), conn, taskDb.TaskId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles")
		}
		tasksDb[i].StudentFiles, err = s.AnswerRepository.TakeFiles(context.TODO(), conn, taskDb.Id)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles")
		}
	}
	tasks = make([]dto.TaskToStudent, 0, len(tasksDb))
	for _, taskDb := range tasksDb {
		var task dto.TaskToStudent
		if task, err = taskDb.ToDto(); err != nil {
			return nil, errors.Wrap(err, "ToDto")
		}
		tasks = append(tasks, task)
	}
	return
}

func (s *TaskService) UploadFileToAnswer(formData multipart.File, answerId int32,
	extension string) (fileLink string, err error) {
	var (
		file       *os.File
		answerIdDb pgtype.Int4
		filenameDb pgtype.Text
	)

	filename := random.String(16, "") + "." + extension
	if err = os.Chdir("/public/answers"); err != nil {
		return "", errors.Wrap(err, "Chdir")
	}
	if file, err = os.Create(filename); err != nil {
		return "", errors.Wrap(err, "Create")
	}
	defer file.Close()
	if _, err = io.Copy(file, formData); err != nil {
		return "", errors.Wrap(err, "Copy")
	}
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return "", errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	if err = filenameDb.Set(filename); err != nil {
		return "", errors.Wrap(err, "Set filename")
	}
	if err = answerIdDb.Set(answerId); err != nil {
		return "", errors.Wrap(err, "Set answerId")
	}
	if fileLink, err = s.AnswerRepository.InsertFileName(context.TODO(), conn, answerIdDb, filenameDb); err != nil {
		return "", errors.Wrap(err, "InsertFileName")
	}
	return
}
