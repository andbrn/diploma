package services

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dto"
	"fmt"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
	"sort"
	"time"
)

type AnswerService struct {
	interfaces.IDBHandler
	AnswerRepository  interfaces.IAnswerRepository
	TaskRepository    interfaces.ITaskRepository
	UserRepository    interfaces.IUserRepository
	SubjectRepository interfaces.ISubjectRepository
}

func (s *AnswerService) UpdateStatus(status string, answerId int32, comment string) (err error) {
	var (
		statusDb   pgtype.Text
		answerIdDb pgtype.Int4
		commentDb  pgtype.Text
	)

	if err = statusDb.Set(status); err != nil {
		return errors.Wrap(err, "Set status")
	}
	if err = answerIdDb.Set(answerId); err != nil {
		return errors.Wrap(err, "Set answerId")
	}
	if err = commentDb.Set(comment); err != nil {
		return errors.Wrap(err, "Set answerId")
	}

	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()

	if err = s.AnswerRepository.ChangeStatus(context.TODO(), conn, statusDb, commentDb, answerIdDb); err != nil {
		return errors.Wrap(err, "ChangeStatus")
	}
	return
}

func (s *AnswerService) SetMark(mark float32, comment string, answerId int32) (err error) {
	var (
		markDb     pgtype.Numeric
		commentDb  pgtype.Text
		answerIdDb pgtype.Int4
	)
	if err = markDb.Set(mark); err != nil {
		return errors.Wrap(err, "Set mark")
	}
	if err = commentDb.Set(comment); err != nil {
		return errors.Wrap(err, "Set comment")
	}
	if err = answerIdDb.Set(answerId); err != nil {
		return errors.Wrap(err, "Set comment")
	}
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return errors.Wrap(err, "AcquireConn")
	}
	if err = s.AnswerRepository.SetMark(context.TODO(), conn, markDb, commentDb, answerIdDb); err != nil {
		return errors.Wrap(err, "SetMark")
	}
	return
}

func (s *AnswerService) TakeJournal(teacherId, groupId, subjectId int32) (journal [][]dto.Cell, err error) {
	var (
		teacherIdDb, groupIdDb, subjectIdDb pgtype.Int4
	)
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()

	if err = teacherIdDb.Set(teacherId); err != nil {
		return nil, errors.Wrap(err, "Set teacherId")
	}
	if err = groupIdDb.Set(groupId); err != nil {
		return nil, errors.Wrap(err, "Set groupId")
	}
	if err = subjectIdDb.Set(subjectId); err != nil {
		return nil, errors.Wrap(err, "Set subjectId")
	}
	teacherDb, err := s.UserRepository.GetTeacherInfo(context.TODO(), conn, teacherIdDb)
	if err != nil {
		return nil, errors.Wrap(err, "GetTeacherInfo")
	}
	teacher, err := teacherDb.ToDto()
	if err != nil {
		return nil, errors.Wrap(err, "ToDto")
	}
	answers, err := s.AnswerRepository.SelectAnswersByGroupAndSubject(context.TODO(), conn, teacherIdDb, groupIdDb, subjectIdDb)
	if err != nil {
		return nil, errors.Wrap(err, "SelectAnswersByGroupAndSubject")
	}
	studentsAnswers := make(map[int32][]dto.Cell)
	journal = make([][]dto.Cell, 0, len(answers))
	dates, err := s.TaskRepository.TakeJournalDates(context.TODO(), conn, teacherIdDb, groupIdDb, subjectIdDb)
	if err != nil {
		return nil, errors.Wrap(err, "TakeDates")
	}
	firstLine := make([]dto.Cell, 0, len(dates))
	firstLine = append(firstLine, dto.Cell{
		FirstCell: true,
		Text:      "Дата",
	})
	for _, date := range dates {
		firstLine = append(firstLine, dto.Cell{
			Text: date.Time.Format("02.01.2006"),
		})
	}
	journal = append(journal, firstLine)
	for _, answer := range answers {
		var (
			studentId int32
			cell      dto.Cell
		)
		if err = answer.PersonId.AssignTo(&studentId); err != nil {
			return nil, errors.Wrap(err, "AssignTo studentId")
		}
		if _, exists := studentsAnswers[studentId]; !exists {
			studentsAnswers[studentId] = make([]dto.Cell, 0, 5)
		}
		cell = dto.Cell{
			FirstCell: false,
			Text:      answer.Text.String,
			Data: &dto.Data{
				Title:       answer.Title.String,
				Description: answer.Description.String,
				Person:      teacher,
				Id:          answer.Id.Int,
				Subtitle:    answer.SubTittle.String,
				StartDate:   answer.StartDate.Time.Format("02.01.2006"),
				EndDate:     answer.EndDate.Time.Format("02.01.2006"),
				Status:      answer.Status.String,
				Comment:     answer.Comment.String,
				Editable:    true,
				Options:     nil,
			},
			Status: answer.Status.String,
			Id:     answer.Id.Int,
		}
		if time.Since(answer.EndDate.Time).Microseconds() > 0 && answer.Status.String != "done" {
			cell.Burned = true
			cell.Data.Burned = true
		}
		options, err := s.TaskRepository.TakeOptions(context.TODO(), conn, answer.StatusId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeOptions")
		}
		cell.Data.Options = make([]dto.Option, 0, len(options))
		for _, option := range options {
			optionDto := dto.Option{}
			if err = option.Action.AssignTo(&optionDto.Action); err != nil {
				return nil, errors.Wrap(err, "AssignTo Action")
			}
			if err = option.Text.AssignTo(&optionDto.Text); err != nil {
				return nil, errors.Wrap(err, "AssignTo Text")
			}
			cell.Data.Options = append(cell.Data.Options, optionDto)
		}
		answerFilesDb, err := s.AnswerRepository.TakeFiles(context.TODO(), conn, answer.Id)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles answer")
		}
		taskFilesDb, err := s.TaskRepository.TakeFiles(context.TODO(), conn, answer.TaskId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles task")
		}
		cell.Data.Files = make([]map[string]string, 0, 3)
		for i, file := range answerFilesDb {
			cell.Data.Files = append(cell.Data.Files, map[string]string{
				"name": fmt.Sprintf("student-%d", i),
				"href": file.String,
			})
		}
		for i, file := range taskFilesDb {
			cell.Data.Files = append(cell.Data.Files, map[string]string{
				"name": fmt.Sprintf("teacher-%d", i),
				"href": file.String,
			})
		}
		studentsAnswers[studentId] = append(studentsAnswers[studentId], cell)
	}
	for k, v := range studentsAnswers {
		studentAnswers := make([]dto.Cell, 0, len(v)+1)
		var studentIdDb pgtype.Int4
		if err = studentIdDb.Set(k); err != nil {
			return nil, errors.Wrap(err, "Set studentIdDb")
		}
		studentDb, err := s.UserRepository.GetStudentInfo(context.TODO(), conn, studentIdDb)
		if err != nil {
			return nil, errors.Wrap(err, "GetStudentInfo")
		}
		studentAnswers = append(studentAnswers, dto.Cell{
			FirstCell: true,
			Text:      studentDb.Name.String,
			Data: &dto.Data{
				Title:       studentDb.Name.String,
				Description: studentDb.Name.String, //TODO описание студента
				Editable:    true,
				Person: dto.Person{
					Name:  studentDb.Name.String,
					Image: studentDb.ImageLink.String,
					Email: studentDb.Email.String,
					Tel:   studentDb.Phone.String,
				},
			},
		})
		studentAnswers = append(studentAnswers, v...)
		journal = append(journal, studentAnswers)
	}
	sort.Sort(ByName(journal[1:]))
	return
}

type ByName [][]dto.Cell

func (n ByName) Len() int {
	return len(n)
}
func (n ByName) Less(i, j int) bool {
	return n[i][0].Data.Person.Name < n[j][0].Data.Person.Name
}
func (n ByName) Swap(i, j int) {
	n[i], n[j] = n[j], n[i]
}

func (s *AnswerService) TakeDiary(studentId int32) (diary [][]dto.Cell, err error) {
	var (
		studentIdDb pgtype.Int4
	)
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()

	if err = studentIdDb.Set(studentId); err != nil {
		return nil, errors.Wrap(err, "Set studentId")
	}

	answers, err := s.AnswerRepository.SelectAnswersByStudent(context.TODO(), conn, studentIdDb)
	if err != nil {
		return nil, errors.Wrap(err, "SelectAnswersByGroupAndSubject")
	}

	diary = make([][]dto.Cell, 0, len(answers))
	dates, err := s.AnswerRepository.TakeDiaryDates(context.TODO(), conn, studentIdDb)
	if err != nil {
		return nil, errors.Wrap(err, "TakeDates")
	}
	firstLine := make([]dto.Cell, 0, len(dates))
	firstLine = append(firstLine, dto.Cell{
		FirstCell: true,
		Text:      "Дата",
	})
	for _, date := range dates {
		firstLine = append(firstLine, dto.Cell{
			Text: date.Time.Format("02.01.2006"),
		})
	}
	diary = append(diary, firstLine)

	answersMap := make(map[int32]map[string]dto.Cell)

	for _, answer := range answers {
		if _, exists := answersMap[answer.SubjectId.Int]; !exists {
			answersMap[answer.SubjectId.Int] = make(map[string]dto.Cell)
		}

		teacherDb, err := s.UserRepository.GetTeacherInfo(context.TODO(), conn, answer.PersonId)
		if err != nil {
			return nil, errors.Wrap(err, "GetTeacherInfo")
		}
		teacher, err := teacherDb.ToDto()
		if err != nil {
			return nil, errors.Wrap(err, "ToDto")
		}

		optionsDb, err := s.TaskRepository.TakeOptions(context.TODO(), conn, answer.StatusId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeOptions")
		}

		options := make([]dto.Option, 0, len(optionsDb))
		for _, option := range optionsDb {
			optionDto := dto.Option{}
			if err = option.Action.AssignTo(&optionDto.Action); err != nil {
				return nil, errors.Wrap(err, "AssignTo Action")
			}
			if err = option.Text.AssignTo(&optionDto.Text); err != nil {
				return nil, errors.Wrap(err, "AssignTo Text")
			}
			options = append(options, optionDto)
		}

		answerFilesDb, err := s.AnswerRepository.TakeFiles(context.TODO(), conn, answer.Id)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles answer")
		}
		taskFilesDb, err := s.TaskRepository.TakeFiles(context.TODO(), conn, answer.TaskId)
		if err != nil {
			return nil, errors.Wrap(err, "TakeFiles task")
		}

		files := make([]map[string]string, 0, 3)
		for i, file := range answerFilesDb {
			files = append(files, map[string]string{
				"name": fmt.Sprintf("student-%d", i),
				"href": file.String,
			})
		}
		for i, file := range taskFilesDb {
			files = append(files, map[string]string{
				"name": fmt.Sprintf("teacher-%d", i),
				"href": file.String,
			})
		}
		var burned bool
		if time.Since(answer.EndDate.Time).Microseconds() > 0 && answer.Status.String != "done" {
			burned = true
		}
		date := answer.StartDate.Time.Format("02.01.2006")
		answersMap[answer.SubjectId.Int][date] = dto.Cell{
			FirstCell: false,
			Text:      answer.Text.String,
			Data: &dto.Data{
				Title:       answer.Title.String,
				Description: answer.Description.String,
				Person:      teacher,
				Id:          answer.Id.Int,
				Subtitle:    answer.SubTittle.String,
				StartDate:   date,
				EndDate:     answer.EndDate.Time.Format("02.01.2006"),
				Status:      answer.Status.String,
				Comment:     answer.Comment.String,
				Editable:    true,
				Options:     options,
				Files:       files,
				Burned:      burned,
			},
			Status: answer.Status.String,
			Id:     answer.Id.Int,
			Burned: burned,
		}

	}

	for subjectId, v := range answersMap {
		line := make([]dto.Cell, 0, len(firstLine))

		var (
			subjectIdDb pgtype.Int4
		)
		if err = subjectIdDb.Set(subjectId); err != nil {
			return nil, errors.Wrap(err, "Set")
		}
		subjectNameDb, err := s.SubjectRepository.GetSubjectName(context.TODO(), conn, subjectIdDb)
		if err != nil {
			return nil, errors.Wrap(err, "GetSubjectName")
		}
		line = append(line, dto.Cell{
			Text:      subjectNameDb.String,
			FirstCell: true,
		})
		for _, date := range firstLine[1:] {
			answer, exists := v[date.Text]
			if !exists {
				line = append(line, dto.Cell{
					Text: "",
				})
				continue
			}
			line = append(line, answer)
		}
		diary = append(diary, line)
	}

	sort.Sort(BySubject(diary[1:]))
	return
}

type BySubject [][]dto.Cell

func (n BySubject) Len() int {
	return len(n)
}
func (n BySubject) Less(i, j int) bool {
	return n[i][0].Text < n[j][0].Text
}
func (n BySubject) Swap(i, j int) {
	n[i], n[j] = n[j], n[i]
}
