package services

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dao"
	"diploma/internal/objects/dto"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
)

type GroupService struct {
	interfaces.IDBHandler
	GroupRepository interfaces.IGroupRepository
}

func (s *GroupService) GetGroup(id int32) (group dto.Group, students []dto.Student, err error) {
	var idDb pgtype.Int4
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return dto.Group{}, nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()

	if err = idDb.Set(id); err != nil {
		return dto.Group{}, nil, errors.Wrap(err, "Scan")
	}
	groupDb, err := s.GroupRepository.GetGroupById(context.TODO(), conn, idDb)
	if err != nil {
		return dto.Group{}, nil, errors.Wrap(err, "GetGroupById")
	}

	studentsDb, err := s.GroupRepository.GetStudentsList(context.TODO(), conn, idDb)
	if err != nil {
		return dto.Group{}, nil, errors.Wrap(err, "GetStudentsList")
	}

	if group, err = groupDb.ToDto(); err != nil {
		return dto.Group{}, nil, errors.Wrap(err, "ToDto group")
	}
	if students, err = dao.StudentsToDto(studentsDb); err != nil {
		return dto.Group{}, nil, errors.Wrap(err, "StudentsToDto")
	}
	return
}

func (s *GroupService) GetGroupsByTeacherId(id int32) (groups []dto.KeyValue, err error) {
	var (
		teacherId pgtype.Int4
	)
	if err = teacherId.Set(id); err != nil {
		return nil, errors.Wrap(err, "Set")
	}
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return nil, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	groupsDb, err := s.GroupRepository.SelectGroupsByTeacherId(context.TODO(), conn, teacherId)
	if err != nil {
		return nil, errors.Wrap(err, "SelectGroupsByTeacherId")
	}
	groups = make([]dto.KeyValue, 0, 6)
	for _, groupDb := range groupsDb {
		group, err := groupDb.ToDto()
		if err != nil {
			return nil, errors.Wrap(err, "ToDto")
		}
		groups = append(groups, group)
	}
	return
}
