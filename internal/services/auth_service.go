package services

import (
	"context"
	"diploma/internal/interfaces"
	"diploma/internal/objects/dto"
	"github.com/jackc/pgtype"
	"github.com/pkg/errors"
	"net/http"
)

type AuthService struct {
	interfaces.IDBHandler
	AuthRepository interfaces.IAuthRepository
}

func (s *AuthService) ProcessAuth(auth dto.Auth) (user dto.User, token string, err error) {
	var emailDb pgtype.Text
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return dto.User{}, "", errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	userId, err := s.AuthRepository.Auth(context.TODO(), conn, auth.Email, auth.Password)
	if err != nil {
		return dto.User{}, "", errors.Wrap(err, "Auth")
	}
	dbToken, err := s.AuthRepository.GenerateToken(context.TODO(), conn, userId)
	if err != nil {
		return dto.User{}, "", errors.Wrap(err, "GenerateToken")
	}
	if err = dbToken.AssignTo(&token); err != nil {
		return dto.User{}, "", errors.Wrap(err, "Scan")
	}
	if err = emailDb.Scan(auth.Email); err != nil {
		return dto.User{}, "", errors.Wrap(err, "Scan email")
	}
	userDb, err := s.AuthRepository.GetInfoByEmail(context.TODO(), conn, emailDb)
	if err != nil {
		return dto.User{}, "", errors.Wrap(err, "ProcessAuth")
	}
	if user, err = userDb.ToDto(); err != nil {
		return dto.User{}, "", errors.Wrap(err, "ToDto")
	}
	return
}

func (s *AuthService) CheckAuth(userId int32, token string) (sc int, isStudent bool, err error) {
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return http.StatusInternalServerError, false, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	var (
		userIdDb pgtype.Int4
		tokenDb  pgtype.UUID
	)
	if err = userIdDb.Set(userId); err != nil {
		return http.StatusBadRequest, false, errors.Wrap(err, "Scan userId")
	}
	if err = tokenDb.Set(token); err != nil {
		return http.StatusBadRequest, false, errors.Wrap(err, "Scan token")
	}
	if sc, err = s.AuthRepository.CheckAuth(context.TODO(), conn, userIdDb, tokenDb); err != nil {
		return sc, false, errors.Wrap(err, "CheckAuth")
	}
	if isStudent, err = s.AuthRepository.IsStudent(context.TODO(), conn, userIdDb); err != nil {
		return http.StatusBadRequest, false, errors.Wrap(err, "IsStudent")
	}
	return
}

func (s *AuthService) UserInfo(userId int32) (user dto.User, err error) {
	var idDb pgtype.Int4
	conn, err := s.AcquireConn(context.TODO())
	if err != nil {
		return dto.User{}, errors.Wrap(err, "AcquireConn")
	}
	defer conn.Release()
	if err = idDb.Set(userId); err != nil {
		return dto.User{}, errors.Wrap(err, "Set")
	}
	userDb, err := s.AuthRepository.GetInfoById(context.TODO(), conn, idDb)
	if err != nil {
		return dto.User{}, errors.Wrap(err, "ProcessAuth")
	}
	if user, err = userDb.ToDto(); err != nil {
		return dto.User{}, errors.Wrap(err, "ToDto")
	}
	return
}
